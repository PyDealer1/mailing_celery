#!/bin/bash
docker-compose exec backend python manage.py makemigrations
docker-compose exec backend python manage.py migrate
docker-compose cp fixtures.json backend:/mailing_app/fixtures.json
docker-compose exec backend python manage.py loaddata fixtures

