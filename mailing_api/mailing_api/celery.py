import os

from django.conf import settings

from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mailing_api.settings')

app = Celery('mailing_api')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.broker_url = settings.CELERY_BROKER_URL

app.conf.beat_schedule = {
    'schedule_task': {
        'task': 'mailing.tasks.check_mailings',
        'schedule': 30.0
    }
}

app.autodiscover_tasks()
