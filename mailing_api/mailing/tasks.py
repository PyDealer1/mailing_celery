import logging
import requests

from django.utils import timezone
from django.db.models import Q

from mailing_api.celery import app
from .models import Mailing, Message, Client
from .api_connect import api_url, headers


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@app.task
def check_mailings():
    return check_mailings_process()


@app.task
def mailing_processing(**kwargs):
    '''Запуск процесса обработки рассылки'''
    mailing_id = kwargs.get('mailing_id')
    process = MailingProcessing(mailing_id)
    return process.get_clients()


@app.task(bind=True, max_retries=3, rate_limit='7/m')
def send_message(self, **kwargs):
    data_for_send = {
        'id': int(kwargs['message_id']),
        'phone': int(kwargs['phone_number']),
        'text': kwargs['message_text'],
    }
    url = f"{api_url}{kwargs['message_id']}"
    try:
        response = requests.post(url, headers=headers, json=data_for_send)
        response.raise_for_status()
        Message.objects.filter(
            id=int(kwargs['message_id'])).update(status='sent')
        logger.info(f"Сообщение {kwargs['message_id']} успешно отправлено.")
        return kwargs

    except requests.exceptions.RequestException as exc:
        if response.status_code == 400:
            Message.objects.filter(
                id=int(kwargs['message_id'])).update(status='re_sent')
            logger.info(
                f"Повторная попытка отправки сообщения {kwargs['message_id']}"
                "через 10 минут.")
            raise self.retry(exc=exc, countdown=600)

        else:
            Message.objects.filter(
                id=int(kwargs['message_id'])).update(status='fail')
            logger.error(
                f"Не удалось отправить сообщение {kwargs['message_id']}."
                f"Ошибка: {exc}")
            raise


def check_mailings_process():
    '''Получает подходящие рассылки'''
    mailings = Mailing.objects.filter(
        start_datetime__lt=timezone.now(),
        end_datetime__gt=timezone.now()
        ).exclude(task_flag='processed')
    if mailings:
        for mailing in mailings:
            mailing.task_flag = 'processed'
            mailing.save()
            mailing_processing.apply_async(
                kwargs={'mailing_id': str(mailing.id)})
        return tuple(mailing.id for mailing in mailings)
    else:
        return 'No mailing lists found'


class MailingProcessing:
    def __init__(self, mailing_id):
        self.mailing_id = mailing_id
        self.mailing = Mailing.objects.get(id=int(mailing_id))
        self.messages_data = {
            'mailing_id': mailing_id,
            'message_text': self.mailing.message_text,
            }
        self.message_ids = []
        self.phone_numbers = []

    def get_clients(self):
        clients = Client.objects.filter(
            Q(tag__in=self.mailing.tag.all()) | Q(
                operator_code__in=self.mailing.operator_code.all(
                ).values_list(
                    'code', flat=True)),
                    ).distinct()
        return self.create_messages(clients)

    def create_messages(self, clients):
        for client in clients:
            message, created = Message.objects.get_or_create(
                mailing_id=self.mailing_id,
                client_id=client.id)
            if created:
                send_message.apply_async(kwargs={
                    'message_id': message.id,
                    'mailing_id': self.messages_data['mailing_id'],
                    'message_text': self.messages_data['message_text'],
                    'phone_number': client.phone_number,
                    })
                message.save()

            self.message_ids.append(message.id)
            self.phone_numbers.append(client.phone_number)
        self.messages_data['phone_number'] = self.phone_numbers
        self.messages_data['message_ids'] = self.message_ids
        return self.messages_data
