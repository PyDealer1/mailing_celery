from rest_framework import viewsets

from .models import Mailing, Client, Message, Tag, OperatorCode
from .serializers import (
    TagSerializer, ClientSerializer, MailingSerializer, MessageSerializer,
    OperatorCodeSerializer, MailingStatisticSerializer)


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class OperatorCodeViewSet(viewsets.ModelViewSet):
    queryset = OperatorCode.objects.all()
    serializer_class = OperatorCodeSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.prefetch_related('tag').all()
    serializer_class = ClientSerializer


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.prefetch_related('operator_code', 'tag').all()
    serializer_class = MailingSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MailingStatisticViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingStatisticSerializer
    http_method_names = ['get']

    def get_queryset(self):
        status = self.request.query_params.get('status', None)
        queryset = Mailing.objects.all()

        if status:
            queryset = queryset.filter(messages__status=status).distinct()
        return queryset
