from django.db import models
from django.utils import timezone as django_tz
from django.core.validators import RegexValidator, MinLengthValidator

from phonenumbers import timezone
import phonenumbers


STATUS_CHOICES = (
    ('active', 'В процессе'),
    ('sent', 'Отправлено'),
    ('re_sent', 'Повторная отправка'),
    ('fail', 'Отправка провалена'),
)


class OperatorCode(models.Model):
    code = models.CharField(
        'Код оператора',
        validators=[MinLengthValidator(limit_value=3)],
        max_length=3,
        unique=True
    )

    def __str__(self):
        return self.code


class Tag(models.Model):
    tag = models.CharField(
        'Тег',
        max_length=20,
        unique=True
        )

    def __str__(self):
        return self.tag


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r'^7\d{10}$',
        message='Формат номера: 71112223344'
        )
    phone_number = models.CharField(
        validators=[phone_regex],
        blank=True,
        unique=True
        )
    operator_code = models.CharField(
        'Код оператора',
        validators=[MinLengthValidator(limit_value=3)],
        max_length=3,
        blank=True,
        null=True,
        help_text=(
            'Заполняется автоматически от номера телефона, '
            'если не указать код оператора явно'
        )
        )
    tag = models.ManyToManyField(
        Tag,
        blank=True,
        related_name='clients'
        )
    time_zone = models.CharField(
        'Часовой пояс',
        max_length=50,
        blank=True,
        help_text=(
            'Заполняется автоматически от номера телефона, '
            'если не указать зону явно'
        )
        )

    def __str__(self):
        return f'Клиент #{self.id} - {self.phone_number}'

    def save(self, *args, **kwargs):
        number = phonenumbers.parse(f'+{self.phone_number}')
        tz = timezone.time_zones_for_geographical_number(number)[0]
        if not self.operator_code:
            self.operator_code = self.phone_number[1:4]
            OperatorCode.objects.get_or_create(code=self.phone_number[1:4])
        else:
            OperatorCode.objects.get_or_create(code=self.operator_code)
        if not self.time_zone:
            self.time_zone = str(tz)
        super().save(*args, **kwargs)


class Mailing(models.Model):
    start_datetime = models.DateTimeField(
        'Дата начала рассылки',
        default=django_tz.now,
        blank=True,
        null=True,
        )
    message_text = models.TextField(
        help_text='Текст сообщения для доставки клиенту')
    operator_code = models.ManyToManyField(
        OperatorCode,
        blank=True,
        related_name='mailings'
        )
    tag = models.ManyToManyField(
        Tag,
        blank=True,
        related_name='mailings'
        )
    end_datetime = models.DateTimeField(
        'Дата и время окончания рассылки',
        null=True
    )

    task_flag = models.CharField(
        blank=True,
        null=True,
        default='not_processed',
        help_text=(
            'Задача обработана или не обработана'
        )
    )

    def __str__(self):
        return f'Рассылка #{self.id} - {self.start_datetime}'


class Message(models.Model):
    creation_datetime = models.DateTimeField(
        'Дата создания сообщения',
        auto_now_add=True
        )
    status = models.CharField(
        'Статус отправки',
        choices=STATUS_CHOICES,
        null=True,
        max_length=20,
        default='active'
        )
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name='messages'
        )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        )

    def __str__(self):
        return (
            f'Сообщение #{self.id} - '
            f'{self.status} - Клиент #{self.client.id}'
        )
