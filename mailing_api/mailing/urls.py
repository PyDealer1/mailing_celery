from rest_framework import routers
from django.urls import include, path

from .views import (
    TagViewSet, ClientViewSet, MailingViewSet, MessageViewSet,
    OperatorCodeViewSet, MailingStatisticViewSet
)


router = routers.DefaultRouter()
router.register(r'tags', TagViewSet)
router.register(r'operator-codes', OperatorCodeViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'mailings', MailingViewSet)
router.register(r'messages', MessageViewSet)
router.register(r'statistic', MailingStatisticViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
