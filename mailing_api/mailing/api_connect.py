import os

from dotenv import load_dotenv

load_dotenv()

jwt_token = os.getenv('JWT')
api_url = os.getenv('API_URL')

headers = {
    'Authorization': f'Bearer {jwt_token}',
    'Content-Type': 'application/json',
}
