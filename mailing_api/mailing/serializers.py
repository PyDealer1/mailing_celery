from rest_framework import serializers
from django.utils import timezone

from .models import Mailing, Client, Message, Tag, OperatorCode, STATUS_CHOICES


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = ('id', 'tag')


class OperatorCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = OperatorCode
        fields = ('id', 'code')


class ClientSerializer(serializers.ModelSerializer):
    tag = serializers.SlugRelatedField(
        many=True,
        slug_field='tag',
        queryset=Tag.objects.all(),
        required=False
    )

    class Meta:
        model = Client
        fields = ('id', 'phone_number', 'operator_code', 'tag', 'time_zone')


class MailingSerializer(serializers.ModelSerializer):
    start_datetime = serializers.DateTimeField(
        format="%Y-%m-%d %H:%M",
        required=False)
    end_datetime = serializers.DateTimeField(format="%Y-%m-%d %H:%M")
    tag = serializers.SlugRelatedField(
        many=True,
        slug_field='tag',
        queryset=Tag.objects.all(),
        required=False
    )
    operator_code = serializers.SlugRelatedField(
        many=True,
        slug_field='code',
        queryset=OperatorCode.objects.all(),
        required=False
    )

    def validate_start_datetime(self, value):
        if value and value < timezone.now():
            raise serializers.ValidationError(
                'Время не может быть установлено раньше текущего')
        return value

    class Meta:
        model = Mailing
        fields = (
            'id', 'start_datetime', 'message_text',
            'operator_code', 'tag', 'end_datetime', 'task_flag')


class MessageSerializer(serializers.ModelSerializer):
    creation_datetime = serializers.DateTimeField(
        format="%Y-%m-%d %H:%M",
        required=False)
    status = serializers.CharField()

    class Meta:
        model = Message
        fields = ('id', 'creation_datetime', 'status', 'mailing', 'client')

    def validate_status(self, value):
        status_values = [status[0] for status in STATUS_CHOICES]
        if value not in status_values:
            raise serializers.ValidationError(
                f'Valid statuses are: {", ".join(status_values)}')
        return value


class MailingStatisticSerializer(serializers.ModelSerializer):
    messages_counts = serializers.SerializerMethodField()
    start_datetime = serializers.DateTimeField(
        format="%Y-%m-%d %H:%M",
        required=False)
    end_datetime = serializers.DateTimeField(format="%Y-%m-%d %H:%M")
    tag = serializers.SlugRelatedField(
        many=True,
        slug_field='tag',
        queryset=Tag.objects.all(),
        required=False
    )
    operator_code = serializers.SlugRelatedField(
        many=True,
        slug_field='code',
        queryset=OperatorCode.objects.all(),
        required=False
    )

    class Meta:
        model = Mailing
        fields = '__all__'

    def get_messages_counts(self, obj):
        return {
            'sent': obj.messages.filter(status='sent').count(),
            'active': obj.messages.filter(status='active').count(),
            'fail': obj.messages.filter(status='fail').count(),
            're_sent': obj.messages.filter(status='re_sent').count(),
        }
