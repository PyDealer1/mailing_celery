# mailing_celery
# Mailing
Cервис управления рассылками.

- За появлением новой рассылки следит celery scheduler: просыпается каждые 30 секунд, берет необработанные рассылки в которых время начала отправки меньше, и время окончания больше чем сейчас.
- Подходящие рассылки по отдельности передаются в очередь. 
- Создается отдельный объект рассылки в рамках задачи celery.
- Формируются данные, с которыми в цикле вызывается задача отправки сообщений.

Реализованные интерфейсы:

API - http://127.0.0.1:8000/docs/

django admin - http://127.0.0.1:8000/admin/

Отслеживание тасок Flowers - http://127.0.0.1:5555/



## Стек
Python 3.9, Django REST Framework, Celery, Redis, Docker, PostgreSQL

## Запуск проекта

Из директории рядом с docker-compose.yml нужно выполнить две команды:

- Запуск проекта

docker compose up --build

- Применение миграций и загрузка фикстур

chmod +x script.sh && ./script.sh

## Фикстуры
Включают в себя объекты клиентов, коды операторов и теги для простоты создания рассылки, т.к. без них рассылка не создастся.

В фикстуры включен пользователь для аутентификации в админке:

Login: qwe

Password: 1212


## Если запуск двумя командами не получился :)

docker compose up --build

docker-compose exec backend python manage.py makemigrations

docker-compose exec backend python manage.py migrate

docker-compose cp fixtures.json backend:/mailing_app/fixtures.json

docker-compose exec backend python manage.py loaddata fixtures


## Дополнительные задания

п3. подготовить docker-compose для запуска всех сервисов проекта одной командой

п5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
